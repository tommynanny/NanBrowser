﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp.WinForms;
using CefSharp;
using System.Linq.Expressions;
using System.Reflection;

namespace NanBrowser
{
	public partial class Browser : MetroFramework.Forms.MetroForm
	{
		ChromiumWebBrowser browser;
		bool isLoading = true;
		String CurrentURL;
		System.Windows.Forms.HtmlDocument CefDom;
		const String StartURL = "google.com";
		public Browser()
		{
			InitializeComponent();
			CefSettings settings = new CefSettings();
			CefSharp.Cef.Initialize(settings);

			browser = new ChromiumWebBrowser(StartURL) { Dock = DockStyle.Fill };
			CefPanel.Controls.Add(browser);

			browser.FrameLoadEnd += WebBrowserFrameLoadEnded;
			browser.FrameLoadStart += WebBrowserFrameLoadStarted;
			browser.AddressChanged += Browser_AddressChanged;
		}

		private void Browser_Load(object sender, EventArgs e)
		{

		}



		private void metroButtonRefresh_Click(object sender, EventArgs e)
		{
			metroButtonNav_Click(this, new EventArgs());
			browser.Refresh();
			URLTextBox.Text = CurrentURL;
		}

		private void metroButtonNav_Click(object sender, EventArgs e)
		{
			browser.Load(URLTextBox.Text);
		}

		private void URLTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				metroButtonNav_Click(this, new EventArgs());
			}
		}

		private void WebBrowserFrameLoadStarted(object sender, FrameLoadStartEventArgs e)
		{
			if (e.Frame.IsMain)
				isLoading = true;
		}

		private void WebBrowserFrameLoadEnded(object sender, FrameLoadEndEventArgs e)
		{
			if (e.Frame.IsMain)
			{
				isLoading = false;
				GetHtmlDocument();
			}
		}

		private void Browser_AddressChanged(object sender, AddressChangedEventArgs e)
		{
			CurrentURL = e.Address;
			URLTextBox.SetPropertyThreadSafe(() => URLTextBox.Text, e.Address);
		}

		private void GetHtmlDocument()
		{

			// browser.IsLoading = True
			if (CefDom == null)
				return;

			// browser.ViewSource()
			browser.GetMainFrame().GetSourceAsync().ContinueWith(taskHtml =>
			{
				var html = taskHtml.Result;
				CefDom.OpenNew(true);
				CefDom.Write(html);
			});
		}

		private void CefPanel_Paint(object sender, PaintEventArgs e)
		{

		}

		private void LogoPictureBox_DoubleClick(object sender, EventArgs e)
		{
			browser.Load("https://tommynanny.com/websites/nan-0/debug");
		}





		//private void Browser_FormClosing(object sender, FormClosingEventArgs e)
		//{
		//	this.Hide();
		//	e.Cancel = true; // this cancels the close event.
		//}

	}
}