﻿namespace NanBrowser
{
	partial class Browser
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Browser));
			this.chromiumWebBrowser1 = new CefSharp.WinForms.ChromiumWebBrowser();
			this.CefPanel = new MetroFramework.Controls.MetroPanel();
			this.metroButtonNav = new MetroFramework.Controls.MetroButton();
			this.metroButtonRefresh = new MetroFramework.Controls.MetroButton();
			this.UpdateTimer = new System.Windows.Forms.Timer(this.components);
			this.URLTextBox = new MetroFramework.Controls.MetroTextBox();
			this.LogoPictureBox = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// chromiumWebBrowser1
			// 
			this.chromiumWebBrowser1.ActivateBrowserOnCreation = false;
			this.chromiumWebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.chromiumWebBrowser1.Location = new System.Drawing.Point(20, 60);
			this.chromiumWebBrowser1.Name = "chromiumWebBrowser1";
			this.chromiumWebBrowser1.Size = new System.Drawing.Size(1003, 696);
			this.chromiumWebBrowser1.TabIndex = 0;
			// 
			// CefPanel
			// 
			this.CefPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CefPanel.HorizontalScrollbarBarColor = true;
			this.CefPanel.HorizontalScrollbarHighlightOnWheel = false;
			this.CefPanel.HorizontalScrollbarSize = 10;
			this.CefPanel.Location = new System.Drawing.Point(20, 60);
			this.CefPanel.Name = "CefPanel";
			this.CefPanel.Size = new System.Drawing.Size(1003, 696);
			this.CefPanel.TabIndex = 1;
			this.CefPanel.VerticalScrollbarBarColor = true;
			this.CefPanel.VerticalScrollbarHighlightOnWheel = false;
			this.CefPanel.VerticalScrollbarSize = 10;
			this.CefPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.CefPanel_Paint);
			// 
			// metroButtonNav
			// 
			this.metroButtonNav.Location = new System.Drawing.Point(490, 23);
			this.metroButtonNav.Name = "metroButtonNav";
			this.metroButtonNav.Size = new System.Drawing.Size(75, 23);
			this.metroButtonNav.TabIndex = 3;
			this.metroButtonNav.Text = "Go";
			this.metroButtonNav.Click += new System.EventHandler(this.metroButtonNav_Click);
			// 
			// metroButtonRefresh
			// 
			this.metroButtonRefresh.Location = new System.Drawing.Point(571, 23);
			this.metroButtonRefresh.Name = "metroButtonRefresh";
			this.metroButtonRefresh.Size = new System.Drawing.Size(75, 23);
			this.metroButtonRefresh.TabIndex = 4;
			this.metroButtonRefresh.Text = "Refresh";
			this.metroButtonRefresh.Click += new System.EventHandler(this.metroButtonRefresh_Click);
			// 
			// UpdateTimer
			// 
			this.UpdateTimer.Enabled = true;
			// 
			// URLTextBox
			// 
			this.URLTextBox.Location = new System.Drawing.Point(82, 23);
			this.URLTextBox.Name = "URLTextBox";
			this.URLTextBox.Size = new System.Drawing.Size(402, 23);
			this.URLTextBox.TabIndex = 5;
			this.URLTextBox.Text = "</http>";
			this.URLTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.URLTextBox_KeyDown);
			// 
			// LogoPictureBox
			// 
			this.LogoPictureBox.Image = global::NanBrowser.Properties.Resources.nan_logo_1;
			this.LogoPictureBox.Location = new System.Drawing.Point(23, 12);
			this.LogoPictureBox.Name = "LogoPictureBox";
			this.LogoPictureBox.Size = new System.Drawing.Size(43, 42);
			this.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.LogoPictureBox.TabIndex = 2;
			this.LogoPictureBox.TabStop = false;
			this.LogoPictureBox.DoubleClick += new System.EventHandler(this.LogoPictureBox_DoubleClick);
			// 
			// Browser
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1043, 776);
			this.Controls.Add(this.LogoPictureBox);
			this.Controls.Add(this.URLTextBox);
			this.Controls.Add(this.metroButtonRefresh);
			this.Controls.Add(this.metroButtonNav);
			this.Controls.Add(this.CefPanel);
			this.Controls.Add(this.chromiumWebBrowser1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "Browser";
			this.Load += new System.EventHandler(this.Browser_Load);
			((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private CefSharp.WinForms.ChromiumWebBrowser chromiumWebBrowser1;
		private MetroFramework.Controls.MetroPanel CefPanel;
		private MetroFramework.Controls.MetroButton metroButtonNav;
		private MetroFramework.Controls.MetroButton metroButtonRefresh;
		private System.Windows.Forms.Timer UpdateTimer;
		private MetroFramework.Controls.MetroTextBox URLTextBox;
		private System.Windows.Forms.PictureBox LogoPictureBox;
	}
}